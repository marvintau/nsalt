package nsalt.fetch.branch

import scala.util._

import org.scalatest._

import chisel3._
import chisel3.util._

import chiseltest._
import chisel3.experimental.BundleLiterals._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class BSW extends Module {
  val io = IO(new Bundle {
    val start = Input(Bool())
    val close = Input(Bool())
    val out = Output(Bool())
  })

  io.out := BoolStopWatch(io.start, io.close, startHighPriority = true)
}

class BoolStopWatchSpec extends AnyFlatSpec with ChiselScalatestTester with Matchers {
  behavior of "BoolStopWatch"

  it should "get the components" in {

    test(new BSW()) { dut =>
    }

  }

}
