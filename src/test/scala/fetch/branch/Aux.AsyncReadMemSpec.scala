package nsalt.fetch.branch

import scala.util._

import org.scalatest._

import chisel3._
import chisel3.util._

import chiseltest._
import chiseltest.experimental._

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import chiselFv._
import nsalt._

class AsyncReadMemSpec extends AnyFlatSpec with ChiselScalatestTester with Matchers with Config {
  behavior of "AsyncReadMem"

  it should "pass the formal verification" in {

    val entrySize = 16

    Check.kInduction(() => new AsyncReadMem(entrySize))

  }

}
