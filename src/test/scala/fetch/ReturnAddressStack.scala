package nsalt.fetch.branch

import scala.util._

import org.scalatest._

import chisel3._
import chisel3.util._

import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import chiselFv._

class ReturnAddressStackSpec extends AnyFlatSpec with ChiselScalatestTester with Matchers {
  behavior of "ReturnAddressStack"

  it should "get the components" in {

    val entryCount = 16

    Check.bmc(() => new ReturnAddressStack(entryCount))

  }

}
