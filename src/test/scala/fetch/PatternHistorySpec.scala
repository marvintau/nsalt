package nsalt.fetch.branch

import scala.util._

import org.scalatest._

import chisel3._
import chisel3.util._

import chiseltest._
import chiseltest.experimental._

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import chiselFv._
import nsalt._

class PatternHistorySpec extends AnyFlatSpec with ChiselScalatestTester with Matchers with Config {
  behavior of "PatternHistory"

  it should "pass the formal verification" in {

    val entrySize = 16
    val entryAddr = new EntryAddr(log2Up(entrySize))

    Check.kInduction(() => new PatternHistory(entrySize, entryAddr))

  }

}
