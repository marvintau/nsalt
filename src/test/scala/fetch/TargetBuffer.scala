package nsalt.fetch.branch

import scala.util._

import org.scalatest._

import chisel3._
import chisel3.util._

import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import chiselFv._

class TargetBufferSpec extends AnyFlatSpec with ChiselScalatestTester with Matchers {
  behavior of "BoolStopWatch"

  it should "get the components" in {

    val entrySize = 16
    val entryAddr = new EntryAddr(log2Up(entrySize))

    Check.bmc(() => new TargetBuffer(entrySize, entryAddr))

  }

}
