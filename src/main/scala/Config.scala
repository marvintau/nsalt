package nsalt

import chisel3._
import chisel3.util._

trait Config {

  val XLEN = 32
  val DATA_BITS  = XLEN 
  val DATA_BYTES = XLEN / 8

  val PADDING_LEN = 2
  val VIRT_MEM_ADDR_LEN = 32 
  val PHYS_ADDR_LEN = 32

  // ADDR_BITS is used at ALU ouput, see ArithLogic module
  val ADDR_BITS = 64

  val M_MODE_ONLY = true
  val DIV_SUPPORT = true

  val RV_COMPRESS = false
}

trait Constants extends Config {
  val CACHE_READ_WIDTH = 8
  val ICACHE_USER_BUNDLE_WIDTH = VIRT_MEM_ADDR_LEN*2 + 9
  val DCACHE_USER_BUNDLE_WIDTH = 16
  val INDEP_BRU = false

  // excerpted from Settings
  // https://github.com/OSCPU/NutShell/blob/fd86beadfc47f52973270ce6109edebd2a30363b/src/main/scala/top/Settings.scala#L23
  val MMIO_BASE = 0x0000000040000000L
  val MMIO_SIZE = 0x0000000040000000L
}

trait Exception {

  def INSTR_ADDR_MISALIGNED = 0
  def INSTR_ACCESS_FAULT    = 1
  def ILLEGAL_INSTR         = 2
  def BREAK_POINT           = 3
  def LOAD_ADDR_MISALIGNED  = 4
  def LOAD_ACCESS_FAULT     = 5
  def STORE_ADDR_MISALIGNED = 6
  def STORE_ACCESS_FAULT    = 7
  def ECALL_U               = 8
  def ECALL_S               = 9
  def ECALL_M               = 11
  def INSTR_PAGE_FAULT      = 12
  def LOAD_PAGE_FAULT       = 13
  def STORE_PAGE_FAULT      = 15

  def ExPrecedence = Seq (
    BREAK_POINT,
    INSTR_PAGE_FAULT,
    INSTR_ACCESS_FAULT,
    ILLEGAL_INSTR,
    INSTR_ADDR_MISALIGNED,
    ECALL_M,
    ECALL_S,
    ECALL_U,
    STORE_ADDR_MISALIGNED,
    LOAD_ADDR_MISALIGNED,
    STORE_PAGE_FAULT,
    LOAD_PAGE_FAULT,
    STORE_ACCESS_FAULT,
    LOAD_ACCESS_FAULT
  )

}
