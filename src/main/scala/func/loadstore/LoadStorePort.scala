package nsalt.func 

import chisel3._
import chisel3.util._
// import chisel3.util.experimental.BoringUtils

import nsalt._
import nsalt.bus._
import nsalt.util._

class LoadStorePort extends FuncUnitPort {

  val dataW = Input(UInt(XLEN.W))
  val instr = Input(UInt(32.W)) // Atom insts need aq rl funct3 bit from instr

  val dmem = new BusUncached(addrBits = VIRT_MEM_ADDR_LEN)
  
  val isMMIO = Output(Bool())

  val dtlbPageFault = Output(Bool()) // TODO: refactor it for new backend
  val loadAddrMisaligned = Output(Bool()) // TODO: refactor it for new backend
  val storeAddrMisaligned = Output(Bool()) // TODO: refactor it for new backend
}

