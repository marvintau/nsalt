package nsalt.func

import chisel3._
import chisel3.util._

import nsalt._
import nsalt.util._

class FuncUnitPort extends Bundle with Config {

  val in = Flipped(Decoupled(new Bundle {
    val src1 = Output(UInt(XLEN.W))
    val src2 = Output(UInt(XLEN.W))
    val oper = Output(OperType())
  }))
  
  val out = Decoupled(Output(UInt(XLEN.W)))

}

