package nsalt.arch 

import chisel3._
import chisel3.util._

import nsalt._

object Priviledged extends InstrType with Config {
  def ECALL       = BitPat("b000000000000_00000_000_00000_1110011")
  def EBREAK      = BitPat("b000000000001_00000_000_00000_1110011")
  def MRET        = BitPat("b001100000010_00000_000_00000_1110011")
  def SRET        = BitPat("b000100000010_00000_000_00000_1110011")
  def SFENCE_VMA  = BitPat("b0001001?????_?????_000_00000_1110011")
  def FENCE       = BitPat("b????????????_?????_000_?????_0001111")
  def WFI         = BitPat("b0001000_00101_00000_000_00000_1110011") 

  val table_s = Array(
    SRET           -> List(InstrI, FuncType.csr, CtrlOperType.jmp),
    SFENCE_VMA     -> List(InstrR, FuncType.mou, MemOrderOperType.sfence_vma)
  )

  val table = Array(
    ECALL          -> List(InstrI, FuncType.csr, CtrlOperType.jmp),
    EBREAK         -> List(InstrI, FuncType.csr, CtrlOperType.jmp),
    MRET           -> List(InstrI, FuncType.csr, CtrlOperType.jmp),
    FENCE          -> List(InstrS, FuncType.mou, MemOrderOperType.fence), // nop    InstrS -> !wen
    WFI            -> List(InstrI, FuncType.alu, ArithLogicOperType.add) // nop
      // FENCE          -> List(InstrB, FuncType.mou, MemOrderOperType.fencei)
  ) ++ (if (!M_MODE_ONLY) table_s else Array())
}
