
package nsalt 

import chisel3._
import chisel3.util._
import chisel3.util.experimental.BoringUtils

trait RegFileParam {
  val REG_NUM = 32
}

class RegFile extends RegFileParam with Config {
  val regfile = Mem(REG_NUM, UInt(XLEN.W))

  def read(addr: UInt) : UInt = {
    Mux(addr === 0.U, 0.U, regfile(addr))
  }

  def write(addr: UInt, data: UInt) = {
    regfile(addr) := data(XLEN-1, 0) 
  }
} 

class ScoreBoard extends RegFileParam {

  val busy = RegInit(0.U(REG_NUM.W))
  
  def isBusy(idx: UInt): Bool = busy(idx)
  
  def mask(idx: UInt) = (1.U(REG_NUM.W) << idx)(REG_NUM-1, 0)
  
  def update(setMask: UInt, clearMask: UInt) = {

    // When clearMask(i) and setMask(i) are both set, setMask(i) wins.
    // This can correctly record the busy bit when reg(i) is written
    // and issued at the same cycle.
    // Note that rf(0) is always free.
    
    busy := Cat(((busy & ~clearMask) | setMask)(REG_NUM-1, 1), 0.U(1.W))
  }
}

