package nsalt.mem

import chisel3._
import chisel3.util._

import nsalt._
import nsalt.bus._

// Translation Lookaside Buffer
// When disabled, behaves like a passthrough, while all page-fault
// signals are set false.

class DummyTransLookaside(implicit val conf: TransLookasideConf) extends Module with TransLookasideConst {

  var io = IO(new TransLookasidePort)

  io.out <> io.in
  io.csrMMU.loadPageFault := false.B
  io.csrMMU.storePageFault := false.B
  io.csrMMU.addr := io.in.req.bits.addr
  io.ipf := false.B
}


object DummyTransLookaside {
  def apply(in: BusUncached, mem: BusUncached, flush: Bool, csrMMU: MemManagePort)(implicit conf: TransLookasideConf) = {

    val tlb = new TransLookaside

    // val tlb = if (enable) {
    //   Module(new EmbeddedTLB)
    // } else {
    //   Module(new EmbeddedTLB_fake)
    // }
    tlb.io.in <> in
    tlb.io.mem <> mem
    tlb.io.flush := flush
    tlb.io.csrMMU <> csrMMU
    tlb
  }
}