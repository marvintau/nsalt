package nsalt.mem

import chisel3._
import chisel3.util._
import chisel3.experimental.ChiselEnum

import nsalt._ 
import nsalt.bus._

// Util for handling flush signal
object FlushReady {
  def apply(setup: Bool, reset: Bool) = {
    val reg = RegInit(false.B)

    when (setup) {
      reg := true.B
    }
    when (reset) {
      reg := false.B
    }

    reg
  }
}

class CacheInstr(implicit val cacheConfig: CacheConf) extends CacheModule {

  val io = IO(new CachePort)

  val flushReady = FlushReady(io.flush(0), io.in.req.fire && !io.flush(0))

  io.in.req.ready := io.out.mem.req.ready
  io.in.res.valid := (io.out.mem.res.valid && !flushReady) || io.flush(0)

  io.in.res.bits.dataR   := io.out.mem.res.bits.dataR
  io.in.res.bits.command := io.out.mem.res.bits.command
  
  val memuser = RegEnable(io.in.req.bits.user.getOrElse(0.U), io.in.req.fire)
  io.in.res.bits.user
    .zip(if (userBits > 0) Some(memuser) else None)
    .map { case (o,i) => o := i }

  io.out.mem.req.bits.apply( 
    addr    = io.in.req.bits.addr,
    command = io.in.req.bits.command,
    size    = io.in.req.bits.size,
    dataW   = io.in.req.bits.dataW,
    maskW   = io.in.req.bits.maskW
  )
  io.out.mem.req.valid := io.in.req.valid
  io.out.mem.res.ready := io.in.res.ready

  io.empty := false.B
  io.mmio := DontCare
  io.out.coh := DontCare
}


object CacheInstr {
  def apply(in: BusUncached, mmio: Seq[BusUncached], flush: UInt, empty: Bool)(implicit cacheConfig: CacheConf) = {

    val cache = Module(new CacheInstr)

    cache.io.flush := flush
    cache.io.in <> in
    mmio(0) <> cache.io.mmio
    empty := cache.io.empty
    cache.io.out
  }
}

object CacheDataState extends ChiselEnum {
  val IDLE, MEM_REQ, MEM_RES, MMIO_REQ, MMIO_RESP, WAIT, Nil = Value
}


class CacheData(implicit val cacheConfig: CacheConf) extends CacheModule{

  import CacheDataState._

  val io = IO(new CachePort)

  val state = RegInit(IDLE)

  val isMMIO = AddressSpace.isMMIO(io.in.req.bits.addr)
  val isMMIOPrev = RegEnable(isMMIO, io.in.req.fire)


  val flushReady = RegInit(false.B)
  when (io.flush(0) && (state =/= IDLE)) {
    flushReady := true.B
  }
  when (state === IDLE && flushReady) { 
    flushReady := false.B 
  }


  val alreadyOutFire = RegEnable(true.B, init = false.B, io.in.res.fire)

  switch (state) {
    is (IDLE) {
      alreadyOutFire := false.B
      when (io.in.req.fire && !io.flush(0)) { state := Mux(isMMIO, MMIO_REQ, MEM_REQ) }
    }
    is (MEM_REQ) {
      when (io.out.mem.req.fire) { state := MEM_RES }
    }
    is (MEM_RES) {
      when (io.out.mem.res.fire) { state := WAIT }
    }
    is (MMIO_REQ) {
      when (io.mmio.req.fire) { state := MMIO_RESP }
    }
    is (MMIO_RESP) {
      when (io.mmio.res.fire || alreadyOutFire) { state := WAIT }
    }
    is (WAIT) {
      when (io.in.res.fire || flushReady || alreadyOutFire) { state := IDLE }
    }
  }

  val addrReq = RegEnable(io.in.req.bits.addr,    io.in.req.fire)
  val command = RegEnable(io.in.req.bits.command, io.in.req.fire)
  val sizeR    = RegEnable(io.in.req.bits.size,    io.in.req.fire)
  val dataW   = RegEnable(io.in.req.bits.dataW,   io.in.req.fire)
  val maskW   = RegEnable(io.in.req.bits.maskW,   io.in.req.fire)

  io.in.req.ready := (state === IDLE)
  io.in.res.valid := (state === WAIT) && (!flushReady)

  val mmioDataR  = RegEnable(io.mmio.res.bits.dataR,      io.mmio.res.fire)
  val mmioComand = RegEnable(io.mmio.res.bits.command,    io.mmio.res.fire)
  val memDataR   = RegEnable(io.out.mem.res.bits.dataR,   io.out.mem.res.fire)
  val memCommand = RegEnable(io.out.mem.res.bits.command, io.out.mem.res.fire)

  io.in.res.bits.dataR   := Mux(isMMIOPrev, mmioDataR,  memDataR)
  io.in.res.bits.command := Mux(isMMIOPrev, mmioComand, memCommand)

  val memuser = RegEnable(io.in.req.bits.user.getOrElse(0.U), io.in.req.fire)
  io.in.res.bits.user
    .zip(if (userBits > 0) Some(memuser) else None)
    .map { case (o,i) => o := i }

  io.out.mem.req.bits.apply(
    addr    = addrReq,
    command = command,
    size    = sizeR,
    dataW   = dataW,
    maskW   = maskW
  )
  io.out.mem.req.valid := (state === MEM_REQ)
  io.out.mem.res.ready := true.B
  
  io.mmio.req.bits.apply(
    addr    = addrReq,
    command = command,
    size    = sizeR,
    dataW   = dataW,
    maskW   = maskW
  )
  io.mmio.req.valid := (state === MMIO_REQ)
  io.mmio.res.ready := true.B

  io.empty := false.B
  io.out.coh := DontCare

}

object CacheData {
  def apply(in: BusUncached, mmio: Seq[BusUncached], flush: UInt, empty: Bool)(implicit cacheConfig: CacheConf) = {

    val cache = Module(new CacheData)

    cache.io.flush := flush
    cache.io.in <> in
    mmio(0) <> cache.io.mmio
    empty := cache.io.empty
    cache.io.out
  }
}
