package nsalt.mem

import chisel3._
import chisel3.util._

import nsalt._
import nsalt.bus._

case class CacheConf (
  ro:         Boolean = false,
  name:       String = "cache",
  userBits:   Int = 0,
  idBits:     Int = 0,
  level:      Int = 1,

  size:       Int = 32, // Kbytes
  ways:       Int = 4
)

trait HasCacheConst {
  implicit val cacheConfig: CacheConf

  val PHYS_ADDR_LEN: Int
  val XLEN: Int

  val cacheName = cacheConfig.name
  val userBits = cacheConfig.userBits
  val idBits = cacheConfig.idBits

  val ro = cacheConfig.ro
  val hasCoh = !ro
  val hasCohInt = if (hasCoh) 1 else 0
  val hasPrefetch = cacheName == "l2cache"
	
  val cacheLevel = cacheConfig.level
  val size = cacheConfig.size

  val Ways = cacheConfig.ways
  val LineSize = XLEN // byte
  val LineBeats = LineSize / 8 //DATA WIDTH 64
  val Sets = size * 1024 / LineSize / Ways
  val OffsetBits = log2Up(LineSize)
  val IndexBits = log2Up(Sets)
  val WordIndexBits = log2Up(LineBeats)
  val TagBits = PHYS_ADDR_LEN - OffsetBits - IndexBits

  val debug = false

  def addrBundle = new Bundle {
    val tag = UInt(TagBits.W)
    val index = UInt(IndexBits.W)
    val wordIndex = UInt(WordIndexBits.W)
    val byteOffset = UInt((if (XLEN == 64) 3 else 2).W)
  }

  def CacheMetaArrayReadBus()  = new SyncMemReadBus(new MetaBundle, sets = Sets, ways = Ways)
  def CacheDataArrayReadBus()  = new SyncMemReadBus(new DataBundle, sets = Sets * LineBeats, ways = Ways)
  def CacheMetaArrayWriteBus() = new SyncMemWriteBus(new MetaBundle, sets = Sets, ways = Ways)
  def CacheDataArrayWriteBus() = new SyncMemWriteBus(new DataBundle, sets = Sets * LineBeats, ways = Ways)

  def getMetaIdx(addr: UInt) = addr.asTypeOf(addrBundle).index
  def getDataIdx(addr: UInt) = Cat(addr.asTypeOf(addrBundle).index, addr.asTypeOf(addrBundle).wordIndex)

  def isSameWord(a1: UInt, a2: UInt) = ((a1 >> 2) === (a2 >> 2))
  def isSetConflict(a1: UInt, a2: UInt) = (a1.asTypeOf(addrBundle).index === a2.asTypeOf(addrBundle).index)
}

// Removed "sealed" for cross-file extending
abstract class CacheBundle(implicit cacheConfig: CacheConf) extends Bundle with Config with HasCacheConst
abstract class CacheModule(implicit cacheConfig: CacheConf) extends Module with Config with HasCacheConst


class CachePort(implicit val cacheConfig: CacheConf) extends CacheBundle {
  val in = Flipped(new BusUncached(userBits = userBits, idBits = idBits))
  val flush = Input(UInt(2.W))
  val mem  = new BusUncached
  val mmio = new BusUncached
  val empty = Output(Bool())

  val out = new BusCached()
}


class MetaBundle(implicit val cacheConfig: CacheConf) extends CacheBundle {
  val tag = Output(UInt(TagBits.W))
  val valid = Output(Bool())
  val dirty = Output(Bool())

  def apply(tag: UInt, valid: Bool, dirty: Bool) = {
    this.tag := tag
    this.valid := valid
    this.dirty := dirty
    this
  }
}

class DataBundle(implicit val cacheConfig: CacheConf) extends CacheBundle {
  val data = Output(UInt(DATA_BITS.W))

  def apply(data: UInt) = {
    this.data := data
    this
  }
}

