package nsalt.mem

import chisel3._
import chisel3.util._

import nsalt._
import nsalt.bus._
import nsalt.util._

class Stage1Port(implicit val cacheConfig: CacheConf) extends CacheBundle {
  val req = new BusReqPort(userBits = userBits, idBits = idBits)
}

// trait HasCachePort {
//   implicit val cacheConfig: CacheConf
//   val io = IO(new CachePort)
// }

// meta read
class CacheStage1(implicit val cacheConfig: CacheConf) extends CacheModule {

  val io = IO(new Bundle {
    val in =
      Flipped(Decoupled(new BusReqPort(userBits = userBits, idBits = idBits)))
    val out = Decoupled(new Stage1Port)
    val metaReadBus = CacheMetaArrayReadBus()
    val dataReadBus = CacheDataArrayReadBus()
  })

  // if (ro) when (io.in.fire) { assert(!io.in.bits.isWrite()) }

  // read meta array and data array
  // Request is being made in this stage. Response shall be return to next stage.
  val readBusValid = io.in.valid && io.out.ready
  io.metaReadBus.apply(
    valid = readBusValid,
    index = getMetaIdx(io.in.bits.addr)
  )
  io.dataReadBus.apply(
    valid = readBusValid,
    index = getDataIdx(io.in.bits.addr)
  )

  io.out.bits.req := io.in.bits
  io.out.valid := io.in.valid && io.metaReadBus.req.ready && io.dataReadBus.req.ready
  io.in.ready := (!io.in.valid || io.out.fire) &&
    io.metaReadBus.req.ready &&
    io.dataReadBus.req.ready

}

class Stage2Port(implicit val cacheConfig: CacheConf) extends CacheBundle {
  val req = new BusReqPort(userBits = userBits, idBits = idBits)
  val metas = Vec(Ways, new MetaBundle)
  val datas = Vec(Ways, new DataBundle)
  val hit = Output(Bool())
  val waymask = Output(UInt(Ways.W))
  val mmio = Output(Bool())
  val isForwardData = Output(Bool())
  val forwardData = Output(CacheDataArrayWriteBus().req.bits)
}

// check
class CacheStage2(implicit val cacheConfig: CacheConf) extends CacheModule {
  val io = IO(new Bundle {
    val in = Flipped(Decoupled(new Stage1Port))
    val out = Decoupled(new Stage2Port)
    val metaReadResp = Flipped(Vec(Ways, new MetaBundle))
    val dataReadResp = Flipped(Vec(Ways, new DataBundle))
    val metaWriteBus = Input(CacheMetaArrayWriteBus())
    val dataWriteBus = Input(CacheDataArrayWriteBus())
  })

  val req = io.in.bits.req
  val addr = req.addr.asTypeOf(addrBundle)

  // forward meta infomation behaves like a switch, and eventually control the
  // request of writing meta information.

  // Tobe changed into a switch pattern

  val isForwardMeta = io.in.valid &&
    io.metaWriteBus.req.valid &&
    io.metaWriteBus.req.bits.index === getMetaIdx(req.addr)

  val isForwardMetaReg = RegInit(false.B)

  when(isForwardMeta) {
    isForwardMetaReg := true.B
  }
  when(io.in.fire || !io.in.valid) {
    isForwardMetaReg := false.B
  }

  val forwardMetaReg = RegEnable(io.metaWriteBus.req.bits, isForwardMeta)

  val metaWay = Wire(Vec(Ways, chiselTypeOf(forwardMetaReg.data)))
  val pickForwardMeta = isForwardMetaReg || isForwardMeta
  val forwardMeta = Mux(isForwardMeta, io.metaWriteBus.req.bits, forwardMetaReg)
  val forwardWaymask = forwardMeta.waymask.getOrElse("1".U).asBools

  forwardWaymask.zipWithIndex.map { case (w, i) =>
    metaWay(i) := Mux(
      pickForwardMeta && w,
      forwardMeta.data,
      io.metaReadResp(i)
    )
  }

  val hitVec = VecInit(
    metaWay.map(m => m.valid && (m.tag === addr.tag) && io.in.valid)
  ).asUInt

  val victimWaymask =
    if (Ways > 1) (1.U << LFSR64()(log2Up(Ways) - 1, 0)) else "b1".U

  val invalidVec = VecInit(metaWay.map(m => !m.valid)).asUInt
  val hasInvalidWay = invalidVec.orR
  val refillInvalidWaymask = Mux(
    invalidVec >= 8.U,
    "b1000".U,
    Mux(
      invalidVec >= 4.U,
      "b0100".U,
      Mux(invalidVec >= 2.U, "b0010".U, "b0001".U)
    )
  )

  // val waymask = Mux(io.out.bits.hit, hitVec, victimWaymask)
  val waymask = Mux(
    io.out.bits.hit,
    hitVec,
    Mux(hasInvalidWay, refillInvalidWaymask, victimWaymask)
  )
  // when(PopCount(waymask) > 1.U){
  //   metaWay.map(m => Debug("[ERROR] metaWay %x metat %x reqt %x\n", m.valid, m.tag, addr.tag))
  //   io.metaReadResp.map(m => Debug("[ERROR] metaReadResp %x metat %x reqt %x\n", m.valid, m.tag, addr.tag))
  //   Debug("[ERROR] forwardMetaReg isForwardMetaReg %x %x metat %x wm %b\n", isForwardMetaReg, forwardMetaReg.data.valid, forwardMetaReg.data.tag, forwardMetaReg.waymask.get)
  //   Debug("[ERROR] forwardMeta isForwardMeta %x %x metat %x wm %b\n", isForwardMeta, io.metaWriteBus.req.bits.data.valid, io.metaWriteBus.req.bits.data.tag, io.metaWriteBus.req.bits.waymask.get)
  // }
  // when(PopCount(waymask) > 1.U){Debug("[ERROR] hit %b wmask %b hitvec %b\n", io.out.bits.hit, forwardMeta.waymask.getOrElse("1".U), hitVec)}
  // assert(!(io.in.valid && PopCount(waymask) > 1.U))

  io.out.bits.metas := metaWay
  io.out.bits.hit := io.in.valid && hitVec.orR
  io.out.bits.waymask := waymask
  io.out.bits.datas := io.dataReadResp
  io.out.bits.mmio := AddressSpace.isMMIO(req.addr)

  val isForwardData = io.in.valid && (io.dataWriteBus.req match {
    case r =>
      r.valid && r.bits.index === getDataIdx(req.addr)
  })

  val isForwardDataReg = RegInit(false.B)
  when(isForwardData) { isForwardDataReg := true.B }
  when(io.in.fire || !io.in.valid) { isForwardDataReg := false.B }
  val forwardDataReg = RegEnable(io.dataWriteBus.req.bits, isForwardData)
  io.out.bits.isForwardData := isForwardDataReg || isForwardData
  io.out.bits.forwardData := Mux(
    isForwardData,
    io.dataWriteBus.req.bits,
    forwardDataReg
  )

  io.out.bits.req <> req
  io.out.valid := io.in.valid
  io.in.ready := !io.in.valid || io.out.fire
  // Debug("[isFD:%d isFDreg:%d inFire:%d invalid:%d \n", isForwardData, isForwardDataReg, io.in.fire, io.in.valid)
  // Debug("[isFM:%d isFMreg:%d metawreq:%x widx:%x ridx:%x \n", isForwardMeta, isForwardMetaReg, io.metaWriteBus.req.valid, io.metaWriteBus.req.bits.index, getMetaIdx(req.addr))

}

// // writeback
sealed class CacheStage3(implicit val cacheConfig: CacheConf)
    extends CacheModule {

  val io = IO(new Bundle {
    val in = Flipped(Decoupled(new Stage2Port))
    val out = Decoupled(new BusResPort(userBits = userBits, idBits = idBits))
    val isFinish = Output(Bool())
    val flush = Input(Bool())
    val dataReadBus = CacheDataArrayReadBus()
    val dataWriteBus = CacheDataArrayWriteBus()
    val metaWriteBus = CacheMetaArrayWriteBus()

    val mem = new BusUncached
    val mmio = new BusUncached
    val cohResp = Decoupled(new BusResPort)

    // use to distinguish prefetch request and normal request
    val dataReadRespToL1 = Output(Bool())
  })

  val metaWriteArb = Module(new Arbiter(CacheMetaArrayWriteBus().req.bits, 2))
  val dataWriteArb = Module(new Arbiter(CacheDataArrayWriteBus().req.bits, 2))

  val req = io.in.bits.req
  val addr = req.addr.asTypeOf(addrBundle)
  val mmio = io.in.valid && io.in.bits.mmio
  val hit = io.in.valid && io.in.bits.hit
  val miss = io.in.valid && !io.in.bits.hit
  val probe = io.in.valid && hasCoh.B && req.isProbe()

  val hitReadBurst = hit && req.isReadBurst()
  val meta = Mux1H(io.in.bits.waymask, io.in.bits.metas)
  // assert(!(mmio && hit), "MMIO request should not hit in cache")

  // this is ugly
  // if (cacheName == "dcache") {
  //   BoringUtils.addSource(mmio, "lsuMMIO")
  // }

  val useForwardData =
    io.in.bits.isForwardData && io.in.bits.waymask === io.in.bits.forwardData.waymask
      .getOrElse("b1".U)
  val dataReadArray = Mux1H(io.in.bits.waymask, io.in.bits.datas).data
  val dataRead =
    Mux(useForwardData, io.in.bits.forwardData.data.data, dataReadArray)
  val wordMask =
    Mux(!ro.B && req.isWrite(), MaskExpand(req.maskW), 0.U(DATA_BITS.W))

  val writeL2BeatCnt = Counter(LineBeats)
  when(
    io.out.fire && (req.command === BusCommand.WRITE_BURST || req.isWriteLast())
  ) {
    writeL2BeatCnt.inc()
  }

  val hitWrite = hit && req.isWrite()
  val dataHitWriteBus = Wire(CacheDataArrayWriteBus()).apply(
    data = Wire(new DataBundle)
      .apply(MaskData(dataRead, req.dataW, wordMask)),
    valid = hitWrite,
    index = Cat(
      addr.index,
      Mux(
        req.command === BusCommand.WRITE_BURST || req.isWriteLast(),
        writeL2BeatCnt.value,
        addr.wordIndex
      )
    ),
    waymask = io.in.bits.waymask
  )

  val metaHitWriteBus = Wire(CacheMetaArrayWriteBus()).apply(
    valid = hitWrite && !meta.dirty,
    index = getMetaIdx(req.addr),
    waymask = io.in.bits.waymask,
    data = Wire(new MetaBundle)
      .apply(tag = meta.tag, valid = true.B, dirty = (!ro).B)
  )

  val s_idle :: s_memReadReq :: s_memReadResp :: s_memWriteReq :: s_memWriteResp :: s_mmioReq :: s_mmioResp :: s_wait_resp :: s_release :: Nil =
    Enum(9)
  val state = RegInit(s_idle)
  val needFlush = RegInit(false.B)

  when(io.flush && (state =/= s_idle)) { needFlush := true.B }
  when(io.out.fire && needFlush) { needFlush := false.B }

  val readBeatCnt = Counter(LineBeats)
  val writeBeatCnt = Counter(LineBeats)

  val s2_idle :: s2_dataReadWait :: s2_dataOK :: Nil = Enum(3)
  val state2 = RegInit(s2_idle)

  io.dataReadBus.apply(
    valid =
      (state === s_memWriteReq || state === s_release) && (state2 === s2_idle),
    index = Cat(
      addr.index,
      Mux(state === s_release, readBeatCnt.value, writeBeatCnt.value)
    )
  )
  val dataWay = RegEnable(io.dataReadBus.res.data, state2 === s2_dataReadWait)
  val dataHitWay = Mux1H(io.in.bits.waymask, dataWay).data

  switch(state2) {
    is(s2_idle) { when(io.dataReadBus.req.fire) { state2 := s2_dataReadWait } }
    is(s2_dataReadWait) { state2 := s2_dataOK }
    is(s2_dataOK) {
      when(io.mem.req.fire || io.cohResp.fire || hitReadBurst && io.out.ready) {
        state2 := s2_idle
      }
    }
  }

  // critical word first read
  val raddr =
    (if (XLEN == 64) Cat(req.addr(PHYS_ADDR_LEN - 1, 3), 0.U(3.W))
     else Cat(req.addr(PHYS_ADDR_LEN - 1, 2), 0.U(2.W)))
  // dirty block addr
  val waddr = Cat(meta.tag, addr.index, 0.U(OffsetBits.W))

  val command = Mux(
    state === s_memReadReq,
    BusCommand.READ_BURST,
    Mux(
      (writeBeatCnt.value === (LineBeats - 1).U),
      BusCommand.WRITE_LAST,
      BusCommand.WRITE_BURST
    )
  )

  io.mem.req.bits.apply(
    addr = Mux(state === s_memReadReq, raddr, waddr),
    command = command,
    size = (if (XLEN == 64) "b11".U else "b10".U),
    dataW = dataHitWay,
    maskW = Fill(DATA_BYTES, 1.U)
  )

  io.mem.res.ready := true.B
  io.mem.req.valid := (state === s_memReadReq) || ((state === s_memWriteReq) && (state2 === s2_dataOK))

  // mmio
  io.mmio.req.bits := req
  io.mmio.res.ready := true.B
  io.mmio.req.valid := (state === s_mmioReq)

  val afterFirstRead = RegInit(false.B)
  val alreadyOutFire = RegEnable(true.B, init = false.B, io.out.fire)
  val readingFirst =
    !afterFirstRead && io.mem.res.fire && (state === s_memReadResp)
  val inRdataRegDemand = RegEnable(
    Mux(mmio, io.mmio.res.bits.dataR, io.mem.res.bits.dataR),
    Mux(mmio, state === s_mmioResp, readingFirst)
  )

  // probe
  io.cohResp.valid := ((state === s_idle) && probe) ||
    ((state === s_release) && (state2 === s2_dataOK))

  io.cohResp.bits.dataR := dataHitWay

  val releaseLast =
    Counter(state === s_release && io.cohResp.fire, LineBeats)._2
  io.cohResp.bits.command := Mux(
    state === s_release,
    Mux(releaseLast, BusCommand.READ_LAST, 0.U),
    Mux(hit, BusCommand.PROBE_HIT, BusCommand.PROBE_MISS)
  )

  val respToL1Fire = hitReadBurst && io.out.ready && state2 === s2_dataOK
  val respToL1Last = Counter(
    (state === s_idle || state === s_release && state2 === s2_dataOK) && hitReadBurst && io.out.ready,
    LineBeats
  )._2

  switch(state) {
    is(s_idle) {
      afterFirstRead := false.B
      alreadyOutFire := false.B

      when(probe) {
        when(io.cohResp.fire) {
          state := Mux(hit, s_release, s_idle)
          readBeatCnt.value := addr.wordIndex
        }
      }.elsewhen(hitReadBurst && io.out.ready) {
        state := s_release
        readBeatCnt.value := Mux(
          addr.wordIndex === (LineBeats - 1).U,
          0.U,
          (addr.wordIndex + 1.U)
        )
      }.elsewhen((miss || mmio) && !io.flush) {
        state := Mux(
          mmio,
          s_mmioReq,
          Mux(!ro.B && meta.dirty, s_memWriteReq, s_memReadReq)
        )
      }
    }

    is(s_mmioReq) { when(io.mmio.req.fire) { state := s_mmioResp } }
    is(s_mmioResp) { when(io.mmio.res.fire) { state := s_wait_resp } }

    is(s_release) {
      when(io.cohResp.fire || respToL1Fire) { readBeatCnt.inc() }
      when(
        probe && io.cohResp.fire && releaseLast || respToL1Fire && respToL1Last
      ) { state := s_idle }
    }

    is(s_memReadReq) {
      when(io.mem.req.fire) {
        state := s_memReadResp
        readBeatCnt.value := addr.wordIndex
      }
    }

    is(s_memReadResp) {
      when(io.mem.res.fire) {
        afterFirstRead := true.B
        readBeatCnt.inc()
        when(req.command === BusCommand.WRITE_BURST) {
          writeL2BeatCnt.value := 0.U
        }
        when(io.mem.res.bits.isReadLast()) { state := s_wait_resp }
      }
    }

    is(s_memWriteReq) {
      when(io.mem.req.fire) { writeBeatCnt.inc() }
      when(io.mem.req.bits.isWriteLast() && io.mem.req.fire) {
        state := s_memWriteResp
      }
    }

    is(s_memWriteResp) { when(io.mem.res.fire) { state := s_memReadReq } }
    is(s_wait_resp) {
      when(io.out.fire || needFlush || alreadyOutFire) { state := s_idle }
    }
  }

  val dataRefill = MaskData(
    io.mem.res.bits.dataR,
    req.dataW,
    Mux(readingFirst, wordMask, 0.U(DATA_BITS.W))
  )
  val dataRefillWriteBus = Wire(CacheDataArrayWriteBus).apply(
    valid = (state === s_memReadResp) && io.mem.res.fire,
    index = Cat(addr.index, readBeatCnt.value),
    data = Wire(new DataBundle).apply(dataRefill),
    waymask = io.in.bits.waymask
  )

  dataWriteArb.io.in(0) <> dataHitWriteBus.req
  dataWriteArb.io.in(1) <> dataRefillWriteBus.req
  io.dataWriteBus.req <> dataWriteArb.io.out

  val metaRefillWriteBus = Wire(CacheMetaArrayWriteBus()).apply(
    valid = (state === s_memReadResp) && io.mem.res.fire && io.mem.res.bits
      .isReadLast(),
    data = Wire(new MetaBundle)
      .apply(valid = true.B, tag = addr.tag, dirty = !ro.B && req.isWrite()),
    index = getMetaIdx(req.addr),
    waymask = io.in.bits.waymask
  )

  metaWriteArb.io.in(0) <> metaHitWriteBus.req
  metaWriteArb.io.in(1) <> metaRefillWriteBus.req
  io.metaWriteBus.req <> metaWriteArb.io.out

  if (cacheLevel == 2) {
    when((state === s_memReadResp) && io.mem.res.fire && req.isReadBurst()) {
      // readBurst request miss
      io.out.bits.dataR := dataRefill
      io.out.bits.command := Mux(
        io.mem.res.bits.isReadLast(),
        BusCommand.READ_LAST,
        BusCommand.READ_BURST
      )
    }.elsewhen(req.isWriteLast() || req.command === BusCommand.WRITE_BURST) {
      // writeBurst/writeLast request, no matter hit or miss
      io.out.bits.dataR := Mux(hit, dataRead, inRdataRegDemand)
      io.out.bits.command := DontCare
    }.elsewhen(hitReadBurst && state === s_release) {
      // readBurst request hit
      io.out.bits.dataR := dataHitWay
      io.out.bits.command := Mux(
        respToL1Last,
        BusCommand.READ_LAST,
        BusCommand.READ_BURST
      )
    }.otherwise {
      io.out.bits.dataR := Mux(hit, dataRead, inRdataRegDemand)
      io.out.bits.command := req.command
    }
  } else {
    io.out.bits.dataR := Mux(hit, dataRead, inRdataRegDemand)
    io.out.bits.command := Mux(
      io.in.bits.req.isRead(),
      BusCommand.READ_LAST,
      Mux(io.in.bits.req.isWrite(), BusCommand.WRITE_RESP, DontCare)
    ) // DontCare, added by lemover
  }
  io.out.bits.user.zip(req.user).map { case (o, i) => o := i }
  io.out.bits.id.zip(req.id).map { case (o, i) => o := i }

  io.out.valid := io.in.valid && Mux(
    req.isBurst() && (cacheLevel == 2).B,
    Mux(
      req.isWrite() && (hit || !hit && state === s_wait_resp),
      true.B,
      (state === s_memReadResp && io.mem.res.fire && req.command === BusCommand.READ_BURST)
    ) || (respToL1Fire && respToL1Last && state === s_release),
    Mux(
      probe,
      false.B,
      Mux(
        hit,
        true.B,
        Mux(
          req.isWrite() || mmio,
          state === s_wait_resp,
          afterFirstRead && !alreadyOutFire
        )
      )
    )
  )

  // With critical-word first, the pipeline registers between
  // s2 and s3 can not be overwritten before a missing request
  // is totally handled. We use io.isFinish to indicate when the
  // request really ends.
  io.isFinish := Mux(
    probe,
    io.cohResp.fire && Mux(
      miss,
      state === s_idle,
      (state === s_release) && releaseLast
    ),
    Mux(
      hit || req.isWrite(),
      io.out.fire,
      (state === s_wait_resp) && (io.out.fire || alreadyOutFire)
    )
  )

  io.in.ready := io.out.ready && (state === s_idle && !hitReadBurst) && !miss && !probe
  io.dataReadRespToL1 := hitReadBurst && (state === s_idle && io.out.ready || state === s_release && state2 === s2_dataOK)

  // assert(!(metaHitWriteBus.req.valid && metaRefillWriteBus.req.valid))
  // assert(!(dataHitWriteBus.req.valid && dataRefillWriteBus.req.valid))
  // assert(!(!ro.B && io.flush), "only allow to flush icache")
}

class Cache(implicit val cacheConfig: CacheConf) extends CacheModule {

  val io = IO(new CachePort)

  // cpu pipeline
  val s1 = Module(new CacheStage1)
  val s2 = Module(new CacheStage2)
  val s3 = Module(new CacheStage3)
  val metaArray = Module(
    new SyncMemArbitrated(
      nRead = 1,
      new MetaBundle,
      set = Sets,
      way = Ways,
    )
  )
  val dataArray = Module(
    new SyncMemArbitrated(
      nRead = 2,
      new DataBundle,
      set = Sets * LineBeats,
      way = Ways
    )
  )

  if (cacheName == "icache") {
    // flush icache when executing fence.i
    val flushICache = WireInit(false.B)
    // BoringUtils.addSink(flushICache, "MOUFlushICache")
    metaArray.reset := reset.asBool || flushICache
  }

  val arb = Module(
    new Arbiter(
      new BusReqPort(userBits = userBits, idBits = idBits),
      hasCohInt + 1
    )
  )
  arb.io.in(hasCohInt + 0) <> io.in.req

  s1.io.in <> arb.io.out
  /*
  val s2BlockByPrefetch = if (cacheLevel == 2) {
      s2.io.out.valid && s3.io.in.valid && s3.io.in.bits.req.isPrefetch() && !s3.io.in.ready
    } else { false.B }
   */
  PipeLink(s1.io.out, s2.io.in, s2.io.out.fire, io.flush(0))
  PipeLink(s2.io.out, s3.io.in, s3.io.isFinish, io.flush(1))
  io.in.res <> s3.io.out
  s3.io.flush := io.flush(1)
  io.out.mem <> s3.io.mem
  io.mmio <> s3.io.mmio
  io.empty := !s2.io.in.valid && !s3.io.in.valid

  io.in.res.valid := Mux(
    s3.io.out.valid && s3.io.out.bits.isPrefetch(),
    false.B,
    s3.io.out.valid || s3.io.dataReadRespToL1
  )

  if (hasCoh) {
    val cohReq = io.out.coh.req.bits
    // coh does not have user signal, any better code?
    val coh = Wire(new BusReqPort(userBits = userBits, idBits = idBits))
    coh.apply(
      addr = cohReq.addr,
      command = cohReq.command,
      size = cohReq.size,
      dataW = cohReq.dataW,
      maskW = cohReq.maskW
    )
    arb.io.in(0).bits := coh
    arb.io.in(0).valid := io.out.coh.req.valid
    io.out.coh.req.ready := arb.io.in(0).ready
    io.out.coh.res <> s3.io.cohResp
  } else {
    io.out.coh.req.ready := true.B
    io.out.coh.res := DontCare
    io.out.coh.res.valid := false.B
    s3.io.cohResp.ready := true.B
  }

  metaArray.io.r(0) <> s1.io.metaReadBus
  dataArray.io.r(0) <> s1.io.dataReadBus
  dataArray.io.r(1) <> s3.io.dataReadBus

  metaArray.io.w <> s3.io.metaWriteBus
  dataArray.io.w <> s3.io.dataWriteBus

  s2.io.metaReadResp := s1.io.metaReadBus.res.data
  s2.io.dataReadResp := s1.io.dataReadBus.res.data
  s2.io.dataWriteBus := s3.io.dataWriteBus
  s2.io.metaWriteBus := s3.io.metaWriteBus

  // if (EnableOutOfOrderExec) {
  //   BoringUtils.addSource(s3.io.out.fire && s3.io.in.bits.hit, "perfCntCondM" + cacheName + "Hit")
  //   BoringUtils.addSource(s3.io.in.valid && !s3.io.in.bits.hit, "perfCntCondM" + cacheName + "Loss")
  //   BoringUtils.addSource(s1.io.in.fire, "perfCntCondM" + cacheName + "Req")
  // }
  // io.in.dump(cacheName + ".in")
  // Debug("InReq(%d, %d) InResp(%d, %d) \n", io.in.req.valid, io.in.req.ready, io.in.res.valid, io.in.res.ready)
  // Debug("{IN s1:(%d,%d), s2:(%d,%d), s3:(%d,%d)} {OUT s1:(%d,%d), s2:(%d,%d), s3:(%d,%d)}\n", s1.io.in.valid, s1.io.in.ready, s2.io.in.valid, s2.io.in.ready, s3.io.in.valid, s3.io.in.ready, s1.io.out.valid, s1.io.out.ready, s2.io.out.valid, s2.io.out.ready, s3.io.out.valid, s3.io.out.ready)
  // when (s1.io.in.valid) { Debug(p"[${cacheName}.S1]: ${s1.io.in.bits}\n") }
  // when (s2.io.in.valid) { Debug(p"[${cacheName}.S2]: ${s2.io.in.bits.req}\n") }
  // when (s3.io.in.valid) { Debug(p"[${cacheName}.S3]: ${s3.io.in.bits.req}\n") }
  // s3.io.mem.dump(cacheName + ".mem")
}

object Cache {
  def apply(
      in: BusUncached,
      mmio: Seq[BusUncached],
      flush: UInt,
      empty: Bool,
      enable: Boolean = true
  )(implicit conf: CacheConf) = {
    val cache = Module(new Cache)
    // val cache = if (enable) Module(new Cache)
    //             else (if (Settings.get("IsRV32"))
    //                     (if (cacheConfig.name == "dcache") Module(new Cache_fake) else Module(new Cache_dummy))
    //                   else
    //                     (Module(new Cache_fake)))
    cache.io.flush := flush
    cache.io.in <> in
    mmio(0) <> cache.io.mmio
    empty := cache.io.empty
    cache.io.out
  }
}
