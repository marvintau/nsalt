
package nsalt 

import chisel3._
import chisel3.util._

import nsalt._
import nsalt.decode._
import nsalt.func._

// Originally WBU, the Write-Back Unit. Found at:
// https://github.com/OSCPU/NutShell/blob/fd86beadfc47f52973270ce6109edebd2a30363b/src/main/scala/nutcore/backend/seq/WBU.scala
// 
// Docs:
// https://oscpu.gitbook.io/nutshell/liu-shui-xian-she-ji-xi-jie/wbu
class WriteBack extends Module with Config{

  val io = IO(new Bundle {
    val in = Flipped(Decoupled(new CommitPort))
    val writeReg = new WriteRegFilePort
    val redirect = new RedirectPort
  })

  io.writeReg.enableW := io.in.bits.decode.ctrlSgnl.regEnableW && io.in.valid
  io.writeReg.dest    := io.in.bits.decode.ctrlSgnl.destRef
  io.writeReg.data    := io.in.bits.commits(io.in.bits.decode.ctrlSgnl.funcType)

  io.in.ready := true.B

  io.redirect := io.in.bits.decode.ctrlFlow.redirect
  io.redirect.valid := io.in.bits.decode.ctrlFlow.redirect.valid && io.in.valid

  val falseWire = WireInit(false.B) // make BoringUtils.addSource happy
 
  // The Content regarding DiffTest & BoringUtils outlets are removed for this moment,
  // since we are not likely using DiffTest through emulator.
}
