package nsalt.decode

import chisel3._
import chisel3.util._

import nsalt._
import nsalt.arch._

class DecodePort extends Bundle {
  val ctrlFlow = new CtrlFlowPort 
  val ctrlSgnl = new CtrlSgnlPort
  val data     = new DataSrcPort
}

class CtrlSgnlPort extends Bundle {
  val src1Type = Output(SrcType())
  val src2Type = Output(SrcType())
  val funcType = Output(FuncType())
  val operType = Output(OperType())

  val src1Ref = Output(UInt(5.W))
  val src2Ref = Output(UInt(5.W))
  val destRef = Output(UInt(5.W))
  
  // TODO: renamed as regEnableW
  val regEnableW = Output(Bool())
  
  val isTrap = Output(Bool())
  val isSrc1Forward = Output(Bool())
  val isSrc2Forward = Output(Bool())
  val noSpecExec = Output(Bool())  // This inst cannot be speculated
  val isBlocked = Output(Bool())   // This inst requires pipeline to be blocked
}


class CommitPort extends Bundle with Config {
  val decode = new DecodePort
  val isMMIO = Output(Bool())
  val intrNO = Output(UInt(XLEN.W))
  val commits = Output(Vec(FuncType.num, UInt(XLEN.W)))
}


class DataSrcPort extends Bundle with Config{
  val src1 = Output(UInt(DATA_BITS.W))
  val src2 = Output(UInt(DATA_BITS.W))
  val imm  = Output(UInt(DATA_BITS.W))
}

