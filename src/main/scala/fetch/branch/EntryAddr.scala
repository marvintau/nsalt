package nsalt.fetch.branch

import chisel3._
import chisel3.util._

import nsalt._

// EntryAddr
// =========
// EntryAddr is a utility bundle for fetching components of an
// address in entry of cache or buffer, especially used in the
// branch target buffer and other modules in instruction fetch
// stage.

// https://github.com/OSCPU/NutShell/blob/fd86beadfc47f52973270ce6109edebd2a30363b/src/main/scala/nutcore/frontend/BPU.scala#L26

class EntryAddr(val idxLen: Int) extends Bundle with Config {
  
  def TAG_LEN = VIRT_MEM_ADDR_LEN - PADDING_LEN - idxLen 

  val tag = UInt(TAG_LEN.W)
  val idx = UInt(idxLen.W)
  val pad = UInt(PADDING_LEN.W)

  def fromUInt(x: UInt) = x
    .asTypeOf(UInt(VIRT_MEM_ADDR_LEN.W))
    .asTypeOf(this)

  def getTag(x: UInt) = fromUInt(x).tag
  def getIdx(x: UInt) = fromUInt(x).idx
}
