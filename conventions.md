# Conventions of Style

Yue Tao

- initial draft 8/25/2022

## Naming

### Single Word

** Abbreviation**

Name identifiers with complete and accurate English words, and avoid using abbreviations. Use abbreviation when a word:

1. **is common concept in the field**
   e.g. `Reg` for register, and `Mem` for memory
2. **is requently used, shared among file, and containing relatively many[*] letters**
   e.g. `addr` for address, `ctrl` for control, and `instr` for instruciton.
3. **is conventional within project/documentation**
   e.g. we agree `Src` for "source operand", and `Dest` for "target operand" (while "dest" for "destination", interchangeable with "target" in context)

When have to use abbreviation, consider the following rules:

1. Avoid using an abbreviation from initial letters of words (actually it is called an "acronym"). Strictly prohibit when using acronym as class/module definition. Use with caution for internal variables.
2. Avoid using the combination of phonetic letters in a word, such as `mgmt` for management.
3. Use affix when possible. Such as `curr` for "current", `prev` for "previous". Try to eliminate ambiguity by using the letter "unique" to the word. For instance, `res` could be either "response", "result", or "resolve" in some specific context. Consider using the whole word instead of affix in such cases. 
