# 重构计划

总体计划

1. 按照新的命名规范重新命名
   1. 缩写/词缀的使用
   2. 统一camel-case/snake-case
2. 按照状态机模型（Moore/Mealy Machine）对模块进行拆分
   1. 对于特定含义的数据/状态有专门的存储机构（Reg/RegFile/Mem）
   2. 读写数据的条件不（或者极少）依赖于其他状态
3. 用端口代替BoringUtils（飞线）
4. 将Module的端口定义由统一的端口文件移至各模块文件内/模块所属namespace
