# Nutshell结构概览（bottom-up）

从自底向上的角度来描述一个硬件系统，并不符合设计一个硬件系统的思路，但有助于我们理解与复现一个既成的系统。

## 存储

### 基础的存储/状态组件

Chisel中可以使用若干种用于存储状态的器件，包括Mem, SyncReadMem和Reg，其中Reg是一个家族，包含RegInit，RegNext以及ShiftRegister(s)等。综合之后反映在Verilog的层次均为reg，只是读写的实现上存在一些差别。

### 在NutShell中构建的状态组件

外部存储器

```Scala
// https://github.com/OSCPU/NutShell/blob/master/src/main/scala/device/AXI4RAM.scala#L40

class RAMHelper(memByte: Int) extends BlackBox with HasNutCoreParameter {
  val io = IO(new Bundle {
    val clk = Input(Clock())
    val rIdx = Input(UInt(DataBits.W))
    val rdata = Output(UInt(DataBits.W))
    val wIdx = Input(UInt(DataBits.W))
    val wdata = Input(UInt(DataBits.W))
    val wmask = Input(UInt(DataBits.W))
    val wen = Input(Bool())
    val en = Input(Bool())
  }).suggestName("io")
}

class AXI4RAM[T <: AXI4Lite](_type: T = new AXI4, memByte: Int,
  useBlackBox: Boolean = false) extends AXI4SlaveModule(_type) with HasNutCoreParameter {

  val offsetBits = log2Up(memByte)
  val offsetMask = (1 << offsetBits) - 1
  def index(addr: UInt) = (addr & offsetMask.U) >> log2Ceil(DataBytes)
  def inRange(idx: UInt) = idx < (memByte / 8).U

  val wIdx = index(waddr) + writeBeatCnt
  val rIdx = index(raddr) + readBeatCnt
  val wen = in.w.fire() && inRange(wIdx)

  val rdata = if (useBlackBox) {
    val mem = Module(new RAMHelper(memByte))
    mem.io.clk := clock
    mem.io.rIdx := rIdx
    mem.io.wIdx := wIdx
    mem.io.wdata := in.w.bits.data
    mem.io.wmask := fullMask
    mem.io.wen := wen
    mem.io.en := true.B
    mem.io.rdata
  } else {
    val mem = Mem(memByte / DataBytes, Vec(DataBytes, UInt(8.W)))

    val wdata = VecInit.tabulate(DataBytes) { i => in.w.bits.data(8*(i+1)-1, 8*i) }
    when (wen) { mem.write(wIdx, wdata, in.w.bits.strb.asBools) }

    Cat(mem.read(rIdx).reverse)
  }

  in.r.bits.data := RegEnable(rdata, ren)
}
```

上述代码中RAMHelper指向了一个BlackBox，可以通过在对应的Verilog module中调用一个verilator所支持的外部函数，从而在仿真过程中模拟读取内存的操作。如果不使用BlackBox，则使用普通的Mem组件。后者会被综合为一个register file。由于我们会使用Chisel自带的工具进行单元测试，我们将使用Mem，并通过工具中的loadMemoryFromFile对内存进行初始化。





