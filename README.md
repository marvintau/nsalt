# NSALT / NutShell Alternative

## 简介

本项目是『[一生一芯](https://ysyx.oscc.cc/)』项目中[NutShell](https://github.com/OSCPU/NutShell)处理器的重构与复现。

本项目旨在对NutShell项目进行学习研究，并适合应用多种基于Chisel语言框架及其他开源仿真综合工具的测试和形式化验证方法。

如有可能，本项目寻求通过各种形式汇入原NutShell项目中进行持续维护。

## 当前进展

本项目当前尚不能提供可以模拟及综合的完整处理器逻辑。对单独组件的测试与形式验证正在进行中。

## 致谢

本项目的原始代码来自[NutShell](https://github.com/OSCPU/NutShell)

本项目中基于Chisel的形式验证平台来自[chiselFV](https://github.com/Moorvan/ChiselFV)

